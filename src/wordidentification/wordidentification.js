var MAX_SCORE = 15,
    sample_card,
    sample_word,
    score = 0;
var canvas, queue, stage;


function selectWord(old_word) {
    var i,
        word;
    do {
        var i = Math.floor(Math.random() * sample_words.length);
        word = sample_words[i];
    } while (word === old_word);
    return word;
}


function setupStage() {
    var labels = buildLabels(sample_word);
    showLabels(labels);

    sample_card = buildCard(sample_word, '');
    sample_card.x = canvas.width / 2;
    sample_card.y = canvas.height / 3;
    stage.addChild(sample_card);
    stage.setChildIndex(sample_card, 0);
}


function randomWords(amount, sample_word) {
    var random_words = [],
        i,
        word;
    while (random_words.length < amount) {
        i = Math.floor(Math.random() * UNIVERSE.length);
        word = UNIVERSE[i];
        if (word[0] !== sample_word[0] &&
                word[word.length - 1] !== sample_word[sample_word.length - 1] &&
                random_words.indexOf(word) === -1) {
            random_words.push(word);
        }
    }
    return random_words;
}


function buildLabels(sample_word) {
    var label,
        labels = [],
        words = randomWords(2, sample_word);
    words.push(sample_word);

    words.forEach(function (word, i) {
        label = buildLabel(word);
        label.name = "label" + i;
        labels.push(label);
    });

    return labels;
}


function showLabels(labels) {
    var i,
        label,
        x = canvas.width / 4;
    while (labels.length > 0) {
        i =  Math.floor(Math.random() * labels.length);
        label = labels[i];
        label.x = x;
        label.homeX = x;
        stage.addChild(label);
        stage.setChildIndex(label, 0);
        x += canvas.width / 4;
        labels.splice(i, 1);
    }
}


function startDrag(e) {
    var label = e.target.parent;
    stage.setChildIndex(label, stage.getNumChildren() - 1);

    stage.addEventListener('stagemousemove', function (e) {
        label.x = e.stageX;
        label.y = e.stageY;
    });

    stage.addEventListener('stagemouseup', function (e) {
        stage.removeAllEventListeners();
        var relpoint = sample_card.globalToLocal(stage.mouseX, stage.mouseY);

        if (label.key === sample_card.key && sample_card.hitTest(relpoint.x, relpoint.y)) {
            label.removeEventListener("mousedown", startDrag);
            incrementScore(2);
            createjs.Tween.get(label).to({x: sample_card.x, y: sample_card.y + 100}, 200, createjs.Ease.quadOut).call(nextWord);
        } else {
            createjs.Tween.get(label).to({x: label.homeX, y: label.homeY}, 200, createjs.Ease.quadOut);
            if (label.key !== sample_card.key) {
                decrementScore();
            } else {
                createjs.Sound.play("so-so", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
            }
        }
    });
}


function nextWord() {
    stage.removeChild(sample_card);
    stage.removeChild(stage.getChildByName("label0"));
    stage.removeChild(stage.getChildByName("label1"));
    stage.removeChild(stage.getChildByName("label2"));

    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            if (VOWELS.indexOf(sample_word[0]) !== -1) {
                window.location.assign('../auraldiscriminationend/' + sample_word[0] + '.html');
            } else {
                var position = ORDER.indexOf(sample_word[0]);
                window.location.assign('../syllablediscrimination1/' + ORDER[position + 1] + '.html');
            }
        });
    } else {
        sample_word = selectWord(sample_word);
        loadMedia([sample_word]);
    }
}


function loadMedia(list) {
    queue = new createjs.LoadQueue();
    queue.addEventListener("complete", setupStage);
    var manifest = [{id: "emptycard", src: "../img/emptycard.png"}];
    list.forEach(function (word) {
        manifest.push({id: word, src : "../img/" + word + ".png"}); // FIXME: asciify src
    });
    queue.loadManifest(manifest);
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    createjs.Ticker.setFPS(60);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Arrastra al dibujo su palabra (puedes fijarte en cómo empieza).");
    loadInstruction(["arrastra_al_dibujo_su_palabra"]);
    showCoinBoard();
    nextWord();
    startGame();
}
