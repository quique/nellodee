var MAX_SCORE = 15,
    sample_card,
    sample_word,
    score = 0;
var canvas, queue, stage;


function selectWord(old_word) {
    var i,
        word;
    do {
        var i = Math.floor(Math.random() * sample_words.length);
        word = sample_words[i];
    } while (word === old_word);
    return word;
}


function setupStage() {
    var i,
        labels = buildLabels(missing_letter),
        xPos = canvas.width / 4,
        yPos = canvas.height * 3 / 4;

    sample_card = buildCard(sample_word, sample_word.substr(0, sample_word.length - 1) + '_');
    sample_card.x = canvas.width / 2;
    sample_card.y = canvas.height / 3;
    stage.addChild(sample_card);
    stage.setChildIndex(sample_card, 0);

    labels = shuffle(labels);
    labels.forEach(function (label, i) {
        label.x = -200;
        label.y = 0;
        label.homeX = xPos;
        label.rotation = Math.random() * 600;
        stage.addChild(label);
        stage.setChildIndex(label, 0);
        createjs.Tween.get(label)
            .wait(i * 100)
            .to({x: xPos, y: yPos, rotation: 0}, 300);
        xPos += canvas.width / 4;
    });
}


function buildLabels(letter) {
    var label,
        labels = [],
        letters = randomVowels(2, letter);
    letters.push(letter);

    letters.forEach(function (letter, i) {
        label = buildLabel(letter);
        label.name = "label" + i;
        labels.push(label);
    });

    return labels;
}


function checkLabel(label) {
    var relpoint = sample_card.globalToLocal(stage.mouseX, stage.mouseY);
    stage.removeAllEventListeners();

    if (label.key === sample_card.key[sample_card.key.length - 1] && sample_card.hitTest(relpoint.x, relpoint.y)) {
        label.removeEventListener("mousedown", startDrag);
        createjs.Tween.get(label).to({x: sample_card.x, y: sample_card.y + 100}, 200, createjs.Ease.quadOut).call(nextWord);
    } else {
        createjs.Tween.get(label).to({x: label.homeX, y: label.homeY}, 200, createjs.Ease.quadOut);
        if (label.key !== sample_card.key[sample_card.key.length - 1]) {
            decrementScore();
        } else {
            createjs.Sound.play("so-so", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        }
    }
}


function startDrag(e) {
    var label = e.target.parent;
    stage.setChildIndex(label, stage.getNumChildren() - 1);

    stage.addEventListener('stagemousemove', function (e) {
        label.x = e.stageX;
        label.y = e.stageY;
    });

    stage.addEventListener('stagemouseup', function (e) {
        checkLabel(label);
    });
}


function nextWord() {
    stage.removeChild(sample_card);
    stage.removeChild(stage.getChildByName("label0"));
    stage.removeChild(stage.getChildByName("label1"));
    stage.removeChild(stage.getChildByName("label2"));

    incrementScore(2);
    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            var position = VOWELS.indexOf(sample_word[sample_word.length - 1]);
            if (position < VOWELS.length) {
                window.location.assign('../auraldiscrimination/' + VOWELS[position + 1] + '.html');
            } else {
                window.location.assign('../missingvowel/index.html');
            }
        });
    } else {
        sample_word = selectWord(sample_word);
        loadMedia([sample_word]);
    }
}


function loadMedia(list) {
    var manifest = [{id: "emptycard", src: "../img/emptycard.png"}];
    list.forEach(function (word) {
        manifest.push({id: word, src : "../img/" + word + ".png"}); // FIXME: asciify src
    });
    queue = new createjs.LoadQueue();
    queue.addEventListener("complete", setupStage);
    queue.loadManifest(manifest);
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    createjs.Ticker.setFPS(60);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    sample_word = selectWord();
    showInstruction("Arrastra al dibujo la letra que falta.");
    loadInstruction(["arrastra_al_dibujo_la_letra_que_falta"]);
    showCoinBoard();
    loadMedia([sample_word]);
    startGame();
}
