var MAX_SCORE = 15,
    random_words,
    sample_card,
    sample_word,
    score = 0;
var canvas, queue, stage;


function checkLetterCard(e) {
    var card = e.currentTarget;
    if (card.key === sample_card.key.slice(0, 2)) {
        incrementScore(1);
        if (score > MAX_SCORE) {
            var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
            instance.on("complete", function () {
                window.location.assign('../worddiscrimination/' + card.key[0] + '.html');
            });
        } else {
            nextWord();
        }
    } else {
        decrementScore();
    }
}


function setupSecondStage(goal_word) {
    var card,
        cards,
        goalcard,
        i,
        xPos = canvas.width / 4,
        yPos = canvas.height * 3 / 4;

    random_words.forEach(function (word) {
        stage.removeChild(stage.getChildByName(word));
    });
    stage.removeChild(sample_card);

    sample_card.x = canvas.width / 3;
    stage.addChild(sample_card);
    stage.setChildIndex(sample_card, 0);

    goalcard = buildCard(goal_word, goal_word);
    goalcard.x = canvas.width * 2 / 3;
    goalcard.y = canvas.height / 3;
    stage.addChild(goalcard);
    stage.setChildIndex(goalcard, 0);

    cards = build2LetterCards(goal_word.slice(0, 2));

    while (cards.length > 0) {
        i =  Math.floor(Math.random() * cards.length);
        card = cards[i];
        card.x = -200;
        card.y = 0;
        card.rotation = Math.random() * 600;
        card.addEventListener('click', checkLetterCard);
        stage.addChild(card);
        stage.setChildIndex(card, 0);
        createjs.Tween.get(card).
            wait(i * 100).
            to({x: xPos, y: yPos, rotation: 0}, 300);
        cards.splice(i, 1);
        xPos += canvas.width / 4;
    }

    stage.removeChild(stage.getChildByName("instruction_label"));
    stage.removeChild(stage.getChildByName("instruction_board"));
    showInstruction("Pulsa la primera sílaba de «" + sample_card.key + "» y «" + goalcard.key + "».");
    createjs.Sound.play("pulsa_las_primeras_letras_de_estos_dibujos", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
}


function check2Initials(e) {
    var card = e.currentTarget;
    if (card.key.slice(0, 2) === sample_card.key.slice(0, 2)) {
        incrementScore(1);
        if (score > MAX_SCORE) {
            var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
            instance.on("complete", function () {
                window.location.assign('../worddiscrimination/' + card.key[0] + '.html');
            });
        } else {
            setupSecondStage(card.key);
        }
    } else {
        decrementScore();
    }
}

function randomWords(amount, sample_word) {
    var i,
        random_words = [],
        word;

    do {
        i = Math.floor(Math.random() * goal_words.length);
        word = goal_words[i];
    } while (word.slice(0, 2) !== sample_word.slice(0, 2) || word === sample_word);
    random_words.push(word);

    while (random_words.length < amount) {
        i = Math.floor(Math.random() * WORDS.length);
        word = WORDS[i];
        if (word.slice(0, 2) !== sample_word.slice(0, 2) &&
                random_words.indexOf(word) === -1) {
            random_words.push(word);
        }
    }
    return random_words;
}


function buildCards() {
    var card,
        cards = [];

    random_words.forEach(function (word) {
        card = buildCard(word, '');
        cards.push(card);
    });

    return cards;
}


function buildSampleCard(sample_word) {
    sample_card = buildCard(sample_word, sample_word);
}


function setupStage() {
    var card,
        cards,
        i,
        xPos = canvas.width / 4,
        yPos = canvas.height * 3 / 4;

    buildSampleCard(sample_word);
    cards = buildCards();

    sample_card.x = canvas.width / 2;
    sample_card.y = canvas.height / 3;
    stage.addChild(sample_card);
    stage.setChildIndex(sample_card, 0);

    while (cards.length > 0) {
        i =  Math.floor(Math.random() * cards.length);
        card = cards[i];
        card.x = -200;
        card.y = 0;
        card.rotation = Math.random() * 600;
        card.addEventListener('click', check2Initials);
        stage.addChild(card);
        stage.setChildIndex(card, 0);
        createjs.Tween.get(card).
            wait(i * 100).
            to({x: xPos, y: yPos, rotation: 0}, 300);
        cards.splice(i, 1);
        xPos += canvas.width / 4;
    }

    stage.removeChild(stage.getChildByName("instruction_label"));
    stage.removeChild(stage.getChildByName("instruction_board"));
    showInstruction("Pulsa el dibujo que empieza igual que «" + sample_word + "».");
    createjs.Sound.play("pulsa_el_dibujo_que_empieza_igual_que_el_de_arriba", createjs.Sound.INTERRUPT_ANY, 0, 0, 0, 1, 0);
}


function selectSampleWord(old_word) {
    var i,
        word;
    do {
        var i = Math.floor(Math.random() * goal_words.length);
        word = goal_words[i];
    } while (word === old_word);
    return word;
}


function nextWord() {
    var four_words;

    random_words.forEach(function (word) {
        stage.removeChild(stage.getChildByName(word));
    });
    stage.removeChild(sample_card);
    stage.removeChild(stage.getChildByName("lettercard0"));
    stage.removeChild(stage.getChildByName("lettercard1"));
    stage.removeChild(stage.getChildByName("lettercard2"));

    sample_word = selectSampleWord(sample_word);
    random_words = randomWords(3, sample_word);
    four_words = [].concat(random_words);
    four_words.push(sample_word);
    loadMedia(four_words);
}


function loadMedia(list) {
    var manifest = [{id: "emptycard", src: "../img/emptycard.png"}];
    list.forEach(function (word) {
        manifest.push({id: word, src : "../img/" + word + ".png"}); // FIXME: asciify src
    });
    queue = new createjs.LoadQueue();
    queue.addEventListener("complete", setupStage);
    queue.loadManifest(manifest);
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    createjs.Ticker.setFPS(60);
}


function instructionsLoaded() {
    var four_words;
    sample_word = selectSampleWord();
    random_words = randomWords(3, sample_word);
    four_words = [].concat(random_words);
    four_words.push(sample_word);
    loadMedia(four_words);
}

function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    loadInstruction(["pulsa_el_dibujo_que_empieza_igual_que_el_de_arriba", "pulsa_las_primeras_letras_de_estos_dibujos"], true);
    showCoinBoard();
    startGame();
}
