var MAX_SCORE = 15,
    sample_square,
    score = 0;
var canvas, queue, stage;


function nextImage() {
    var animal,
        i,
        instance;

    stage.removeChild(sample_square);
    for (i = 0; i < 8; ++i) {
        stage.removeChild(stage.getChildByName("square" + i));
    }

    if (score > MAX_SCORE) {
        instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../orientation/index.html');
        });
    } else {
        animal = selectItem(ANIMALS, animal);
        setupStage(animal);
    }
}


function checkSquare(e) {
    var clicked_square = e.target.parent;
    if (clicked_square.name === sample_square.name) {
        stage.setChildIndex(clicked_square, stage.getNumChildren() - 1);
        incrementScore(2);
        createjs.Tween.get(clicked_square).to({x: sample_square.x, y: sample_square.y}, 400, createjs.Ease.quadOut).call(nextImage);
    } else {
        decrementScore();
    }
}


function buildSquares(animal) {
    var angle = 0,
        i,
        mirror,
        squares = [];

    for (i = 0; i < 8; ++i) {
        mirror = (i >= 4);
        square = buildSquare(animal, angle, mirror);
        square.name = "square" + i;
        squares.push(square);
        angle += 90;
    }
    squares = shuffle(squares);
    return squares;
}


function setupStage(animal) {
    var squares = buildSquares(animal),
        xPos = canvas.width / 5,
        yPos = canvas.height / 2;

    sample_square = squares[Math.floor(Math.random() * squares.length)].clone(true);
    sample_square.x = canvas.width / 2;
    sample_square.y = canvas.height / 4;
    stage.addChild(sample_square);
    stage.setChildIndex(sample_square, 0);

    squares.forEach(function (square, i) {
        square.x = -200;
        square.y = 0;
        square.homeX = xPos;
        square.rotation = Math.random() * 600;
        stage.addChild(square);
        stage.setChildIndex(square, 0);
        createjs.Tween.get(square)
            .wait(i * 100)
            .to({x: xPos, y: yPos, rotation: 0}, 300);
        xPos += canvas.width / 5;
        if (i === 3) {
            xPos = canvas.width / 5;
            yPos = canvas.height * 3 / 4;
        }
    });
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    createjs.Ticker.setFPS(50);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Pulsa el dibujo que es como el de arriba.");
    loadInstruction(["pulsa_el_dibujo_que_es_como_el_de_arriba"]);
    showCoinBoard();
    nextImage();
    startGame();
}
