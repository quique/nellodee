var COLORS = ['red', 'orange', 'yellow', 'green', 'aqua', 'blue', 'purple'];
var SHAPES = [star, star, triangle, triangle, circle, circle, square, square, rectangle, rectangle];
var MAX_SCORE = 15;
var FACTOR = 1.5;

var canvas, stage;
var counter = 0;
var score = 0;
var shape;


function showShape(counter) {
    var c = Math.floor(Math.random() * COLORS.length),
        color = COLORS[c];
    shape = SHAPES[counter](color, FACTOR);
    shape.x = 0;
    shape.y = Math.floor(Math.random() * (canvas.height - 250)) + 125;
    shape.addEventListener('click', newShape);
    stage.addChild(shape);
}


function updateShape() {
    shape.x += counter + 1;
    if (shape.x >= canvas.width) {
        shape.x = 0;
        shape.y = Math.floor(Math.random() * (canvas.height - 250)) + 125;
    }
}


function newShape(e) {
    stage.removeChild(e.target);
    incrementScore(2);
    counter += 1;

    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../draganddrop/index.html');
        });
    } else {
        showShape(counter);
    }
}


function tick(event) {
    updateShape();
    stage.update(event);
}


function startGame() {
    //createjs.Ticker.timingMode = createjs.Ticker.RAF;
    createjs.Ticker.addEventListener("tick", tick);
    createjs.Ticker.setFPS(50);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Pulsa sobre la figura.");
    loadInstruction(["pulsa_sobre_la_figura"]);
    showCoinBoard();
    showShape(counter);

    startGame();
}
