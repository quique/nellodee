var MAX_SCORE = 15,
    cards = [],
    goal_card,
    random_words,
    sample_word,
    score = 0;
var canvas, queue, stage;


function randomWords(amount, sample_word) {
    var i,
        word,
        random_words = [];
    while (random_words.length < amount) {
        i = Math.floor(Math.random() * UNIVERSE.length);
        word = UNIVERSE[i];
        if (word[0] !== sample_word[0] &&
                word[word.length - 1] !== sample_word[sample_word.length - 1] &&
                random_words.indexOf(word) === -1) {
            random_words.push(word);
        }
    }
    return random_words;
}


function selectSampleWord(old_word) {
    var i,
        word;
    do {
        var i = Math.floor(Math.random() * goal_words.length);
        word = goal_words[i];
    } while (word === old_word);
    return word;
}


function buildCards(random_words, sample_word) {
    var i,
        card,
        cards = [];
    for (i = 0; i < random_words.length; ++i) {
        card = buildCard(random_words[i], random_words[i]);
        cards.push(card);
    }
    goal_card = buildCard(sample_word, sample_word);
    cards.push(goal_card);
    return cards;
}


function showGoalLabel() {
    var label = buildLabel(sample_word);
    label.x = canvas.width / 2;
    label.homeX = canvas.width / 2;
    label.name = "sample_label";
    stage.addChild(label);
}


function startDrag(e) {
    var label = e.target.parent;
    stage.setChildIndex(label, stage.getNumChildren() - 1);

    stage.addEventListener('stagemousemove', function (e) {
        label.x = e.stageX;
        label.y = e.stageY;
    });

    stage.addEventListener('stagemouseup', function (e) {
        stage.removeAllEventListeners();
        var relpoint = goal_card.globalToLocal(stage.mouseX, stage.mouseY);

        if (goal_card.hitTest(relpoint.x, relpoint.y)) {
            label.removeEventListener("mousedown", startDrag);
            createjs.Tween.get(label).to({x: goal_card.x, y: goal_card.y + 100}, 200, createjs.Ease.quadOut).call(nextWord);
        } else {
            createjs.Tween.get(label).to({x: label.homeX, y: label.homeY}, 200, createjs.Ease.quadOut);
            createjs.Sound.play("so-so", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        }
    });
}


function setupStage() {
    var i,
        card,
        cards_clone,
        xPos = canvas.width / 4,
        yPos = canvas.height / 3;

    showGoalLabel();
    cards = buildCards(random_words, sample_word);
    cards_clone = cards.concat();

    while (cards_clone.length > 0) {
        i =  Math.floor(Math.random() * cards_clone.length);
        card = cards_clone[i];
        card.x = -200;
        card.y = 0;
        card.rotation = Math.random() * 600;
        stage.addChild(card);
        createjs.Tween.get(card)
            .wait(i * 100)
            .to({x: xPos, y: yPos, rotation: 0}, 300);
        xPos += canvas.width / 4;
        cards_clone.splice(i, 1);
    }

    showInstruction("Arrastra la palabra «" + sample_word + "» a su dibujo (puedes fijarte en cómo empieza).");
}


function nextWord() {
    var three_words;
    stage.removeChild(stage.getChildByName("instruction_label"));
    stage.removeChild(stage.getChildByName("instruction_board"));
    stage.removeChild(stage.getChildByName("sample_label"));
    cards.forEach(function (card) {
        stage.removeChild(card);
    });

    incrementScore(2);
    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            if (VOWELS.indexOf(goal_card.key[0]) !== -1) {
                window.location.assign('../missinginitial/' + goal_card.key[0] + '.html');
            } else {
                window.location.assign('../missing2initials/' + goal_card.key[0] + '.html');
            }
        });
    } else {
        sample_word = selectSampleWord(sample_word);
        random_words = randomWords(2, sample_word);
        three_words = [].concat(random_words);
        three_words.push(sample_word);
        loadMedia(three_words);
    }
}


function loadMedia(list) {
    var manifest = [{id: "emptycard", src: "../img/emptycard.png"}];
    list.forEach(function (word) {
        manifest.push({id: word, src : "../img/" + word + ".png"}); // FIXME: asciify src
    });
    queue = new createjs.LoadQueue();
    queue.addEventListener("complete", setupStage);
    queue.loadManifest(manifest);
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    createjs.Ticker.setFPS(60);
}


function init() {
    var three_words;

    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    sample_word = selectSampleWord('');
    random_words = randomWords(2, sample_word);
    three_words = [].concat(random_words);
    three_words.push(sample_word);
    loadMedia(three_words);

    showCoinBoard();
    loadInstruction(["arrastra_la_palabra_a_su_dibujo"]);
    startGame();
}
