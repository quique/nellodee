var canvas, queue, stage,
    images = ["cartilla_ilustrada", "preparacion", "mayusculas", "minusculas", "info", "vocales", "consonantes"];


function buildRoundLabel(word, url, zoom, bgcolor) {
    var board, label, labelText;
    zoom = zoom || 1;
    bgcolor = bgcolor || '#ff0000';
    label = new createjs.Container();

    board = new createjs.Shape();
    board.graphics.beginStroke('#000');
    board.graphics.beginFill(bgcolor);
    board.graphics.drawCircle(0, 0, 100 * zoom);
    //board.regX = 90;
    //board.regY = 25;
    board.x = board.y = 0;
    label.addChild(board);

    labelText = new createjs.Text(word, "32px Arial", "#fff");
    labelText.textBaseline = "middle";
    labelText.textAlign = 'center';
    labelText.x = 0;
    labelText.y = 0;
    //labelText.regX = 0;
    label.addChild(labelText);

    label.url = url;
    label.name = word;
    label.regX = 0;
    label.regY = 0;
    label.y = canvas.height / 2;

    label.cursor = 'default';
    label.on('rollover', function () {
        label.cursor = 'pointer';
        board.alpha = 0.4;
        stage.update();
    });
    label.on('rollout', function () {
        label.cursor = 'default';
        board.alpha = 1;
        stage.update();
    });

    return label;
}


function showExplanation(e) {
    var message = e.currentTarget.explanation,
        container = new createjs.Container(),
        board = new createjs.Shape(),
        label = new createjs.Text(message, "28px Arial", "#222");

    stage.getChildByName('back').visible = false;

    board.alpha = 0.5;
    board.graphics.beginFill('#BBBBBB');
    board.graphics.drawRect(0, 0, canvas.width * 0.8, 60);
    board.regX = canvas.width * 0.4;
    board.regY = 30;
    container.addChild(board);

    label.textAlign = 'center';
    label.textBaseline = "middle";
    container.addChild(label);

    container.name = "explanation";
    container.x = canvas.width / 2;
    container.y = canvas.height * 4 / 5;
    stage.addChild(container);

    stage.update();
}


function redirect(e) {
    var url = e.currentTarget.url;
    window.location.assign(url);
}


function showCircle(text, url, x, explanation) {
    var circle = buildRoundLabel(text, url, 0.5);
    circle.x = x;
    circle.addEventListener("click", redirect);
    circle.addEventListener("mouseover", function (e) {
        showExplanation(explanation);
    });
    circle.addEventListener("mouseout", function (e) {
        stage.removeChild(stage.getChildByName("explanation"));
    });
    stage.addChild(circle);
    stage.update();
}

function preparation() {
    var actions = ["activity01", "activity02", "draganddrop", "visualdiscrimination", "visualdiscrimination2", "sizes", "sizes2", "memory", "orientation2", "orientation"];
    loadImages(actions, preparation2, "screenshots/", ".jpg");
}

function preparation2() {
    var actions = ["activity01", "activity02", "draganddrop", "visualdiscrimination", "visualdiscrimination2", "sizes", "sizes2", "memory", "orientation2", "orientation"],
        explanations = [
            "Actividad de pulsaciones",
            "Pulsaciones sobre figuras en movimiento",
            "Arrastrar y soltar",
            "Discriminación de formas",
            "Discriminación inversa de formas",
            "Discriminación de tamaños",
            "Discriminación inversa de tamaños",
            "Memoria",
            "Orientación espacial: comparación",
            "Orientación espacial: nombres"
        ],
        bmp;
    stage.removeAllChildren();

    bmp = imageLink("img/preparacion.png", 125, 100, canvas.width / 2, canvas.height / 5);
    bmp.removeAllEventListeners();
    stage.addChild(bmp);

    showScreenShots(actions, explanations, "index");

    bmp = imageLink("img/left_arrow2.png", 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", function () {
        loadImages(images, mainMenu, "img/")
    });
    bmp.name = "back";
    stage.addChild(bmp);

    stage.update();
}


function showScreenShots(actions, explanations, letter) {
    var bmp,
        gap = canvas.width / 4,
        midX = canvas.width / 2,
        midY = canvas.height / 2,
        shape,
        shots = [],
        x;

    shape = new createjs.Shape();
    shape.graphics.beginStroke('#009');
    shape.graphics.drawRoundRect(0, 0, 276, 180, 12);
    shape.regX = 138;
    shape.regY = 90;
    shape.x = canvas.width / 2;
    shape.y = canvas.height / 2;
    stage.addChild(shape);

    actions.forEach(function (action, i) {
        x = midX + i * gap;
        bmp = imageLink("screenshots/" + action + ".jpg", 128, 80, x, midY);
        bmp.url = action + "/" + letter + ".html",
        bmp.explanation = explanations[i];
        if (x === midX) {
            bmp.addEventListener("click", redirect);
            bmp.addEventListener("mouseover", showExplanation);
        } else {
            bmp.addEventListener("click", function (e) {
                moveShots(shots, e.stageX);
            });
        }
        bmp.addEventListener("mouseout", function (e) {
            stage.removeChild(stage.getChildByName("explanation"));
            stage.getChildByName('back').visible = true;
        });
        shots.push(bmp);
        stage.addChild(bmp);
    });
}


function moveShots(shots, x) {
    var gap = x > canvas.width / 2 ? -canvas.width / 4 : canvas.width / 4,
        newX;

    shots.forEach( function (shot) {
        newX = shot.x + gap
        if (shot.x === canvas.width / 2) {
            shot.removeEventListener("click", redirect);
            shot.removeEventListener("mouseover", showExplanation);
            shot.addEventListener("click", function (e) {
                moveShots(shots, e.stageX);
            });
        } else if (newX === canvas.width / 2) {
            shot.addEventListener("click", redirect);
            shot.addEventListener("mouseover", showExplanation);
        }
        createjs.Tween.get(shot).to({x: newX}, 200, createjs.Ease.quadOut);
    });
}


function vowel(e) {
    var letter = e.target.parent.name.toLowerCase(),
        actions = ["auraldiscrimination", "worddiscrimination", "missinginitial", "wordidentification"],
        explanations = [
            "Discriminación auditiva de la vocal inicial",
            "Discriminación de palabras por su vocal inicial",
            "Identificación de la vocal inicial que falta",
            "Identificación de palabras por su vocal inicial"
        ],
        bmp;

    // At the moment there are not words ending in "i" nor "u"
    if (['a', 'e', 'o'].indexOf(letter) !== -1) {
        actions.push("auraldiscriminationend");
        explanations.push("Discriminación auditiva de la vocal final");
        actions.push("missingfinal");
        explanations.push("Identificación de la vocal final que falta");
    }

    stage.removeAllChildren();

    var letter2 = letter;
    if (getCookie("case") === "uppercase") {
        letter2 = letter.toUpperCase();
    }

    var circle = buildRoundLabel(letter2, '#', 0.5);
    circle.x = canvas.width / 2;
    circle.y = canvas.height / 5;
    circle.removeAllEventListeners();
    stage.addChild(circle);

    showScreenShots(actions, explanations, letter);

    bmp = imageLink("img/left_arrow2.png", 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", vowels);
    bmp.name = "back";
    stage.addChild(bmp);

    stage.update();
}


function vowelReview(e) {
    var actions = ["missingvowel"],
        bmp,
        circle,
        explanations = ["Identificación de la vocal inicial que falta"],
        shape;

    stage.removeAllChildren();

    circle = buildRoundLabel("Todas", '#', 0.5);
    circle.x = canvas.width / 2;
    circle.y = canvas.height / 5;
    circle.removeAllEventListeners();
    stage.addChild(circle);

    showScreenShots(actions, explanations, "index");

    bmp = imageLink("img/left_arrow2.png", 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", vowels);
    bmp.name = "back";
    stage.addChild(bmp);

    stage.update();
}


function vowels(e) {
    var circle,
        letters = ["a", "e", "i", "o", "u"],
        SEPARATION = canvas.width / (letters.length + 2),
        x = SEPARATION;
    stage.removeAllChildren();

    if (getCookie("case") === "uppercase") {
        letters = letters.map(function (letter) {
            return letter.toUpperCase();
        });
    }

    bmp = imageLink(queue.getResult("vocales"), 125, 100, canvas.width / 2, canvas.height / 5);
    bmp.removeAllEventListeners();
    stage.addChild(bmp);


    letters.forEach(function (letter) {
        circle = buildRoundLabel(letter, '#', 0.5);
        circle.x = x;
        circle.addEventListener("click", vowel);
        stage.addChild(circle);
        x += SEPARATION;
    });

    circle = buildRoundLabel("Todas", '#', 0.5);
    circle.x = x;
    circle.addEventListener("click", vowelReview);
    stage.addChild(circle);

    bmp = imageLink("img/left_arrow2.png", 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", letterClass);
    bmp.name = "back";
    stage.addChild(bmp);

    stage.update();
}


function consonant(e) {
    var letter = e.target.parent.name.toLowerCase(),
        actions = ["syllablediscrimination1", "worddiscrimination", "missing2initials", "wordidentification"],
        explanations = [
            "Discriminación auditiva de la sílaba inicial",
            "Discriminación de palabras por su sílaba inicial",
            "Identificación de la sílaba que falta",
            "Identificación de palabras por su sílaba inicial"
        ],
        bmp,
        shots = [];

    stage.removeAllChildren();


    var letter2 = letter;
    if (getCookie("case") === "uppercase") {
        letter2 = letter.toUpperCase();
    }

    var circle = buildRoundLabel(letter2, '#', 0.5);
    circle.x = canvas.width / 2;
    circle.y = canvas.height / 5;
    circle.removeAllEventListeners();
    stage.addChild(circle);

    showScreenShots(actions, explanations, letter);

    bmp = imageLink("img/left_arrow2.png", 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", consonants);
    bmp.name = "back";
    stage.addChild(bmp);

    stage.update();
}


function consonants(e) {
    var circle,
        letters = ["p", "s", "m", "l", "t", "n", "d", "r", "v", "b"],
        SEPARATION = canvas.width / 9,
        x = SEPARATION;
        y = canvas.height / 2;
    stage.removeAllChildren();

    if (getCookie("case") === "uppercase") {
        letters = letters.map(function (letter) {
            return letter.toUpperCase();
        });
    }

    bmp = imageLink(queue.getResult("consonantes"), 125, 100, canvas.width / 2, canvas.height / 5);
    bmp.removeAllEventListeners();
    stage.addChild(bmp);

    letters.forEach(function (letter) {
        circle = buildRoundLabel(letter, '#', 0.5);
        circle.x = x;
        circle.y = y;
        circle.addEventListener("click", consonant);
        stage.addChild(circle);
        x += SEPARATION;
        if (x >= SEPARATION * 8) {
            x = SEPARATION;
            y += SEPARATION;
        }
    });

    bmp = imageLink("img/left_arrow2.png", 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", letterClass);
    bmp.name = "back";
    stage.addChild(bmp);

    stage.update();
}


function info() {
    var bmp,
        labelText;
    stage.removeAllChildren();

    bmp = imageLink(queue.getResult("cartilla_ilustrada"), 375, 48, canvas.width / 2, canvas.height / 5);
    bmp.removeAllEventListeners();
    // bmp.addEventListener("click", mainMenu);
    stage.addChild(bmp);

    labelText = new createjs.Text("http://www.cartilla.xyz\n\n\nEnrique Matías Sánchez\n<cronopios@gmail.com>", "48px Handlee", "#111");
    labelText.textBaseline = "center";
    labelText.textAlign = 'center';
    labelText.x = canvas.width / 2;
    labelText.y = canvas.height / 3;
    stage.addChild(labelText);

    bmp = imageLink("img/left_arrow2.png", 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", function () {
        loadImages(images, mainMenu, "img/")
    });
    bmp.name = "back";
    stage.addChild(bmp);

    stage.update();
}


function letterClass() {
    var bmp;
    stage.removeAllChildren();

    if (getCookie("case") === "uppercase") {
        bmp = imageLink(queue.getResult("mayusculas"), 125, 100, canvas.width / 2, canvas.height / 5);
    } else {
        bmp = imageLink(queue.getResult("minusculas"), 125, 100, canvas.width / 2, canvas.height / 5);
    }
    bmp.removeAllEventListeners();
    stage.addChild(bmp);


    bmp = imageLink(queue.getResult("vocales"), 125, 100, canvas.width / 3, canvas.height / 2);
    bmp.addEventListener("click", vowels);
    stage.addChild(bmp);

    bmp = imageLink(queue.getResult("consonantes"), 125, 100, canvas.width * 2 / 3, canvas.height / 2);
    bmp.addEventListener("click", consonants);
    stage.addChild(bmp);


    bmp = imageLink("img/left_arrow2.png", 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", function () {
        loadImages(images, mainMenu, "img/")
    });
    bmp.name = "back";
    stage.addChild(bmp);

    stage.update();
}


function uppercase() {
    setCookie("case", "uppercase", 365);
    letterClass();
}


function lowercase() {
    setCookie("case", "lowercase", 365);
    letterClass();
}


function imageLink(img, regX, regY, x, y) {
    var bmp = new createjs.Bitmap(img);
    bmp.regX = regX;
    bmp.regY = regY;
    bmp.x = x;
    bmp.y = y;
    bmp.cursor = 'default';
    bmp.on('rollover', function () {
        bmp.cursor = 'pointer';
        bmp.alpha = 0.4;
        stage.update();
    });
    bmp.on('rollout', function () {
        bmp.cursor = 'default';
        bmp.alpha = 1;
        stage.update();
    });
    return bmp;
}


function mainMenu() {
    var bmp;
    stage.removeAllChildren();

    bmp = new createjs.Bitmap(queue.getResult("cartilla_ilustrada"));
    bmp.regX = 375;
    bmp.regY = 48;
    bmp.x = canvas.width / 2;
    bmp.y = canvas.height / 5;
    stage.addChild(bmp);

    bmp = imageLink(queue.getResult("preparacion"), 125, 100, canvas.width / 4, canvas.height / 2);
    bmp.addEventListener("click", preparation);
    stage.addChild(bmp);

    bmp = imageLink(queue.getResult("mayusculas"), 125, 100, canvas.width / 2, canvas.height / 2);
    bmp.addEventListener("click", uppercase);
    stage.addChild(bmp);

    bmp = imageLink(queue.getResult("minusculas"), 125, 100, canvas.width * 3 / 4, canvas.height / 2);
    bmp.addEventListener("click", lowercase);
    stage.addChild(bmp);

    bmp = imageLink(queue.getResult("info"), 50, 50, canvas.width / 2, canvas.height * 4 / 5);
    bmp.addEventListener("click", info);
    stage.addChild(bmp);

    stage.update();
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    stage.enableMouseOver();
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });

    loadImages(images, mainMenu, "img/");
}
