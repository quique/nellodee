var COLORS = ['red', 'orange', 'yellow', 'green', 'aqua', 'blue', 'purple'];
var MAX_SCORE = 15;

var canvas, stage;
var slots = [];
var score = 0;


function showLayout() {
    var shapes = [square, rectangle, circle, star, triangle],
        factors = [1, 1.5, 2],
        tempfactors = [].concat(factors),
        c = Math.floor(Math.random() * COLORS.length),
        s = Math.floor(Math.random() * shapes.length),
        f = Math.floor(Math.random() * factors.length),
        piece = shapes[s](COLORS[c], factors[f]),
        i,
        slot;

    piece.x = canvas.width / 2;
    piece.y = canvas.height * 3 / 4;
    piece.homeX = piece.x;
    piece.homeY = piece.y;
    piece.addEventListener("mousedown", startDrag);
    stage.addChild(piece);
    stage.setChildIndex(piece, 0);

    for (i = 0; i < factors.length; ++i) {
        f = Math.floor(Math.random() * tempfactors.length);
        slot = shapes[s]('white', tempfactors[f]);
        tempfactors.splice(f, 1);
        slot.x = (i + 1) * canvas.width / (factors.length + 1);
        slot.y = canvas.height / 4;
        stage.addChild(slot);
        stage.setChildIndex(slot, 0);
        slots.push(slot);
        if (slot.factor === piece.factor) {
            piece.slotNr = i;
            piece.name = "sample";
        }
    }
}


function startDrag(e) {
    var shape = e.target,
        slot;
    stage.setChildIndex(shape, stage.getNumChildren() - 1);

    stage.addEventListener('stagemousemove', function (e) {
        shape.x = e.stageX;
        shape.y = e.stageY;
    });

    stage.addEventListener('stagemouseup', function (e) {
        stage.removeAllEventListeners();
        slot = slots[shape.slotNr];
        var relpoint = slot.globalToLocal(stage.mouseX, stage.mouseY);

        if (slot.hitTest(relpoint.x, relpoint.y)) {
            shape.removeEventListener("mousedown", startDrag);
            createjs.Tween.get(shape).to({x: slot.x, y: slot.y}, 200, createjs.Ease.quadOut).call(newPiece);
        } else {
            createjs.Tween.get(shape).to({x: shape.homeX, y: shape.homeY}, 200, createjs.Ease.quadOut);
            createjs.Sound.play("so-so", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        }
    });
}


function newPiece(e) {
    slots.forEach(function (slot) {
        stage.removeChild(slot);
    });
    stage.removeChild(stage.getChildByName("sample"));

    incrementScore(2);
    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../sizes2/index.html');
        });
    } else {
        showLayout();
    }
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    //createjs.Ticker.setFPS(60);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Arrastra la figura a su hueco.");
    loadInstruction(["arrastra_la_figura_a_su_hueco2"]);
    showCoinBoard();
    showLayout();

    startGame();
}
