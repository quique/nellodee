var MAX_SCORE = 15;
var FACTOR = 1.5;

var canvas, stage;
var pieces = [];
var slot;
var score = 0;


function showLayout() {
    var shapes = [square, rectangle, circle, star, triangle],
        tempshapes = [].concat(shapes),
        colors = ['red', 'orange', 'green', 'aqua', 'blue', 'purple', 'teal'],
        sl = Math.floor(Math.random() * shapes.length),
        i,
        s,
        c,
        piece;

    slot = shapes[sl]('white', FACTOR);
    slot.x = canvas.width / 2;
    slot.y = canvas.height / 4;
    stage.addChild(slot);
    stage.setChildIndex(slot, 0);

    for (i = 0; i < shapes.length; ++i) {
        s = Math.floor(Math.random() * tempshapes.length);
        c = Math.floor(Math.random() * colors.length);
        piece = tempshapes[s](colors[c], FACTOR);
        tempshapes.splice(s, 1);
        colors.splice(c, 1);
        piece.x = (i + 1) * canvas.width / (shapes.length + 1);
        piece.y = canvas.height * 3 / 4;
        piece.homeX = piece.x;
        piece.homeY = piece.y;
        piece.addEventListener("mousedown", startDrag);
        stage.addChild(piece);
        stage.setChildIndex(piece, 0);
        pieces.push(piece);
    }
}


function startDrag(e) {
    var shape = e.target;
    stage.setChildIndex(shape, stage.getNumChildren() - 1);

    stage.addEventListener('stagemousemove', function (e) {
        shape.x = e.stageX;
        shape.y = e.stageY;
    });

    stage.addEventListener('stagemouseup', function (e) {
        stage.removeAllEventListeners();
        var relpoint = slot.globalToLocal(stage.mouseX, stage.mouseY);

        if (shape.name === slot.name && slot.hitTest(relpoint.x, relpoint.y)) {
            shape.removeEventListener("mousedown", startDrag);
            createjs.Tween.get(shape).to({x: slot.x, y: slot.y}, 200, createjs.Ease.quadOut).call(newPiece);
        } else {
            createjs.Tween.get(shape).to({x: shape.homeX, y: shape.homeY}, 200, createjs.Ease.quadOut);
            if (shape.name !== slot.name) {
                decrementScore();
            } else {
                createjs.Sound.play("so-so", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
            }
        }
    });
}


function newPiece(e) {
    pieces.forEach(function (piece) {
        stage.removeChild(piece);
    });
    stage.removeChild(slot);

    incrementScore(2);
    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../sizes/index.html');
        });
    } else {
        showLayout();
    }
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    //createjs.Ticker.setFPS(60);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Arrastra al hueco la figura con su misma forma.");
    loadInstruction(["arrastra_al_hueco_la_figura_con_su_misma_forma"]);
    showCoinBoard();
    showLayout();

    startGame();
}
