var ALL_WORDS = {
    'a': ["abanico", "abeja", "aceite", "agua", "aguacate", "ajo", "alas",
            "alicates", "amigo", "anillos", "araña", "autobús", "avión"],
    'b': ["balalaika", "balanza", "ballena", "balón", "barril", "batería", "batido",
            "bebé", "berenjena",
            "biberón", "bicicleta", "bigote", "billete",
            "boca", "bocadillo", "bolígrafo", "bolo", "bota", "bote", "botella", "botón",
            "bufanda", "burro", "buzón"],
    'c': ["caballo", "café", "cafetera", "caja", "calabaza", "cama", "camello",
            "camión", "camiseta", "caña", "cañón", "caracol", "caravana", "carroza", "casa",
            "cebolla", "cebra", "cenicero", "cepillo", "cerezas", "cerilla",
            "cine",
            "cocinero", "coco", "cohete", "conejo", "copa", "corona",
            "cubo", "cubos", "cuchara", "cuchillo", "cuna"],
    'ch': ["chistera", "chocolate"],
    'd': ["dado", 
            "dedo", "desayuno", 
            "diamante", "diana", "dinero", "dinosaurio", 
            "dominó"],
    'e': ["elefante", "enanito", "erizo", "etiqueta"],
    'f': ["foca"],
    'g': ["gallina", "gafas", "gallo", "gato", 
            "gorila", "gorra", 
            "guacamayo", "guantes", "guitarra", "gusano"],
    'h': ["hacha", "hada", "herradura", "higo", "hoguera", "hojas", "hucha", "hueso", "huevo"],
    'i': ["iglesia", "iglú", "isla"],
    'j': ["jirafa"],
    'k': ["karate", "kayak", "kiwi"],
    'l': ["labios", "lana", "lápiz", "lata", "lavabo", "lavadora", "lazo", 
            "leche", "lechuza", "león",
            "libélula", "libro", "liebre", "limón", "litera",
            "lobo", "loco", "loro",
            "luna", "lupa"],
    'll': ["llama", "llave"],
    'm': ["madalena", "madera", "mago", "maíz", "maleta", "mamá", "mano", "mapa", "marinero", "mariposa", "mariquita",
            "mejillón", "melocotón", "melón", "mesa", "metro", "miel",
            "mochila", "moneda", "molino", "momia", "mono", "monociclo", "monopatín", "moto",
            "muñeca"],
    'n': ["naranja", "nariz", "navaja", "navidad",
            "nevera",
            "nido",
            "noria", "novios",
            "nube", "nudo", "nuez"],
    'ñ': [],
    'o': ["oca", "ojo", "ola", "oreja", "oruga", "oso", "oveja"],
    'p': ["paja", "pajarita", "pala", "palo", "paloma", "palomitas", "papá", "papaya", "paracaídas",
            "paraguas", "patatas", "patinete", "pato", "pavo", "payaso",
            "peine", "pelo", "pelota", "pepino", "pera", "periódico", "perro", "pesas",
            "piano", "pico", "pie", "pila", "pimiento", "piña", "pipa",
            "pirata", "piruleta", "pito", "pizarra",
            "policía", "pollo", "polo", "pomelo", "pozo", "puente", "puño", "puro"],
    'q': ["queso",],
    'r': ["rábano", "radio", "rama", "raqueta", "ratón",
            "regadera", "regalo", "regla", "reina", "reloj", "reno", "revista", "rey", 
            "rinoceronte",
            "robot", "roca", "romano", "rosa", "rotulador",
            "rueda"],
    's': ["sacapuntas", "saco", "salero", "sapo",
            "sello", "semáforo", "seta",
            "sierra", "silla", "sillón",
            "sobre", "sofá", "sopa",
            "suelo"],
    't': ["taza",
            "té", "tele", "teléfono", "tenedor", "tesoro", "tetera",
            "tiburón", "tierra", "tigre", "tijeras", "timón", "tirachinas",
            "toalla", "tobogán", "tomate", "torero", "toro", "torre", 
            "tucán", "túnel"],
    'u': ["unicornio", "uña", "uvas"],
    'v': ["vaca", "valla", "vaso",
            "velas", "velero", "veneno",
            "vikingo", "vino", "violín"],
    'w': [],
    'x': [],
    'y': [],
    'z': ["zanahoria", "zapatillas", "zapato", "zorro", "zumo"],
    'xn': ["ancla"],
    'xm': ["ambulancia"],
    'xr': ["ordenador"],
    'xs': ["escalera"],
    'xv': ["ovni"],
    'xx': ["excavadora", "extintor"],
    'xxb': ["submarinista"],
    'xxc': ["cactus"],
    'xxl': ["bolsa", "bolso", "delfín", "salvavidas", "sol", "volcán"],
    'xxm': ["bombero", "bombilla", "campana", "hamburguesa", "sombrero"],
    'xxn': ["bandera", "banjo", "bonsai", "candado", "cangrejo", "fantasma",
            "lancha", "linterna", "manzana", "pan", "panda", "pingüino",
            "pintor", "pinza", "sandalias", "sandía", "sandwich", "tangram", "ventana"],
    'xxr': ["barca", "barco", "cerveza", "corcho", "dardo", "furgoneta", "hormiga",
            "mortero", "sartén", "serpiente", "tarta", "tornado", "tortuga"],
    'xxs': ["báscula", "casco", "castillo", "despertador", "vestido"],
    'xxt': ["fútbol"],
    'br': ["bruja", "brújula"],
    'cl': ["clip"],
    'dr': ["dragón", "dromedario"],
    'fl': ["flamencos"],
    'fr': ["fresa"],
    'gl': ["globo", "globos"],
    'gr': ["grifo"],
    'pl': ["plancha", "plátano", "plato"],
    'tr': ["tractor", "tranvía", "tren"]
};


var VOWELS = ['a', 'e', 'o', 'i', 'u'];

var ORDER = [
    'p', 's', 'm', 'l', 't',
    'n', 'd', 'r', 'v', 'b',
    'c', 'f', 'll', 'j', 'g', 'ñ', 'ch', 'z', 'q', 'k', 'x', 'w', 'y', 'h'
]

var ALPHABET = VOWELS.concat(ORDER);


var WORDS = [];
ALPHABET.forEach(function (k) {
    WORDS = WORDS.concat(ALL_WORDS[k]);
});

var WORDS_1 = [].
    concat(ALL_WORDS['p']).
    concat(ALL_WORDS['s']).
    concat(ALL_WORDS['m']).
    concat(ALL_WORDS['l']).
    concat(ALL_WORDS['t']);

var WORDS_2 = [].
    concat(ALL_WORDS['n']).
    concat(ALL_WORDS['d']).
    concat(ALL_WORDS['r']).
    concat(ALL_WORDS['v']).
    concat(ALL_WORDS['b']);

var SYLLABLES_1 = [
    "pa", "pe", "pi", "po", "pu",
    "sa", "se", "si", "so", "su",
    "ma", "me", "mi", "mo", "mu",
    "ta", "te", "ti", "to", "tu",
    "la", "le", "li", "lo", "lu"
];

var SYLLABLES_2 = [
    "na", "ne", "ni", "no", "nu",
    "da", "de", "di", "do", "du",
    "ra", "re", "ri", "ro", "ru",
    "va", "ve", "vi", "vo", "vu",
    "ba", "be", "bi", "bo", "bu"
];


var ANIMALS = ["araña", "ballena", "burro", "caballo", "camello", "caracol", "cebra", "cocodrilo",
    "conejo", "delfín", "dragón", "dromedario", "erizo", "flamencos", "foca",
    "gallina", "gallo", "gato", "guacamayo", "hormiga", "jirafa", "lechuza", "león", "libélula",
    "liebre", "lobo", "loro", "llama", "mariquita", "mejillón", "mono", "oca", "oruga", "oso",
    "oveja", "paloma", "panda", "pato", "pavo",
    "perro", "pingüino", "pollo", "rata", "ratón", "reno", "rinoceronte",
    "serpiente", "toro", "tortuga", "tucán", "unicornio", "zorro"];

var FRUITS = ["aguacate", "cerezas", "coco", "fresa", "higo", "limón", "manzana",
    "melocotón", "melón", "naranja", "papaya", "pera", "piña", "plátano",
    "pomelo", "sandía", "tomate", "uvas"];
