var sound_manifest = [
    {src: "../sounds/crash.ogg", id: "error", data: 2},
    // {src:"../sounds/smudge.ogg", id: "so-so", data: 2},
    {src: "../sounds/apert2.ogg", id: "so-so", data: 2},
    {src: "../sounds/level.ogg", id: "success", data: 2},
    {src: "../sounds/bonus.ogg", id: "big-success", data: 2}
];

createjs.Sound.initializeDefaultPlugins();
var q = new createjs.LoadQueue();
q.installPlugin(createjs.Sound);
createjs.Sound.alternateExtensions = ["mp3"];
//createjs.Sound.registerManifest(sound_manifest, "sounds/");
q.loadManifest(sound_manifest);
