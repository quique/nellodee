function randomItems(amount, excluded, universe) {
    var i, item,
        random_items = [];

    while (random_items.length < amount) {
        i = Math.floor(Math.random() * universe.length);
        item = universe[i];
        if (item !== excluded &&
                random_items.indexOf(item) === -1) {
            random_items.push(item);
        }
    }
    return random_items;
}


function randomLetters(amount, sample_letter) {
    var random_letters = randomItems(amount, sample_letter, ALPHABET);
    return random_letters;
}


function randomVowels(amount, sample_vowel) {
    var random_vowels = randomItems(amount, sample_vowel, VOWELS);
    return random_vowels;
}


function randomSyllables1(amount, sample_syllable) {
    var random_syllables = randomItems(amount, sample_syllable, SYLLABLES);
    return random_syllables;
}


function buildCard(word, text) {
    var card, bmp, label, textfont;
    card = new createjs.Container();

    bmp = new createjs.Bitmap(queue.getResult('emptycard'));
    bmp.shadow = new createjs.Shadow("#666", 5, 5, 10);
    card.regX = bmp.image.width / 2;  // 104
    card.regY = bmp.image.height / 2; // 147
    card.addChild(bmp);

    bmp = new createjs.Bitmap(queue.getResult(word));
    bmp.regX = bmp.image.width / 2;
    bmp.regY = bmp.image.height / 2;
    bmp.x = card.regX;
    bmp.y = 104; // 8 + 192/2
    card.addChild(bmp);

    if (getCookie("case") === "uppercase") {
        text = text.toUpperCase();
        textfont = "30px Handlee";
    } else {
        textfont = "40px Handlee";
    }

    label = new createjs.Text(text, textfont, "#222222");
    label.textAlign = 'center';
    label.x = card.regX;
    label.y = 232;
    card.addChild(label);

    card.key = word;
    card.name = word;
    return card;
}


function buildLetterCard(letter, size) {
    var card, bmp, label;
    size = size || 120;
    card = new createjs.Container();

    bmp = new createjs.Bitmap(queue.getResult('emptycard'));
    bmp.shadow = new createjs.Shadow("#666", 5, 5, 10);
    card.regX = bmp.image.width / 2;
    card.regY = bmp.image.height / 2;
    card.addChild(bmp);

    card.key = letter;
    if (getCookie("case") === "uppercase") {
        letter = letter.toUpperCase();
    } else {
        size *= 1.2;
    }
    label = new createjs.Text(letter, size + "px Handlee", "#222222");
    label.textAlign = 'center';
    label.x = card.regX;
    label.y = 44; // was: 120 - size;
    card.addChild(label);

    return card;
}


function build2LetterCards(syllable) {
    var card,
        cards = [],
        syllables = randomSyllables1(2, syllable);
    syllables.push(syllable);

    syllables.forEach(function (syllable, i) {
        card = buildLetterCard(syllable, 100);
        card.name = "lettercard" + i;
        cards.push(card);
    });

    return cards;
}


function buildLabel(word) {
    var board, label, labelText;
    label = new createjs.Container();

    board = new createjs.Shape();
    board.alpha = 0.8;
    board.graphics.beginStroke('#000');
    board.graphics.beginFill('#ffffff');
    board.graphics.drawRect(0, 0, 270, 75);
    board.regX = 135;
    board.regY = 37;
    board.x = board.y = 0;
    label.addChild(board);

    label.key = word;
    label.name = word;
    if (getCookie("case") === "uppercase") { word = word.toUpperCase(); }

    labelText = new createjs.Text(word, "45px Handlee", "#222");
    labelText.textBaseline = "middle";
    labelText.textAlign = 'center';
    labelText.x = 0;
    labelText.y = 0;
    //labelText.regX = 0;
    label.addChild(labelText);

    label.regX = 0;
    label.regY = 0;
    label.y = canvas.height * 3 / 4;
    label.homeY = canvas.height * 3 / 4;
    label.addEventListener("mousedown", startDrag);

    return label;
}


function buildSquare(filename, angle, mirror) {
    var bg, bmp, img, square;
    square = new createjs.Container();
    square.regX = 92; // 184 / 2
    square.regY = 92;
    square.key = filename;
    square.shadow = new createjs.Shadow("#666", 5, 5, 10);
    square.addEventListener("click", checkSquare);

    bg = new createjs.Shape();
    bg.alpha = 0.8;
    bg.graphics.beginStroke('#000');
    bg.graphics.beginFill('#ffffff');
    bg.graphics.drawRect(0, 0, 184, 184);
    square.addChild(bg);

    img = new Image();
    img.src = '../img/' + filename + '.png';
    bmp = new createjs.Bitmap(img);
    window.setTimeout(function () {}, 100);
    bmp.regX = bmp.image.width / 2 || 88;
    bmp.regY = bmp.image.height / 2 || 88;
    bmp.rotation = angle;
    if (mirror) { bmp.scaleX = -1; }
    bmp.x = square.regX;
    bmp.y = square.regY;
    bmp.name = "fg";
    square.addChild(bmp);

    return square;
}


function showInstruction(message) {
    var board, label;

    board = new createjs.Shape();
    board.alpha = 0.5;
    board.graphics.beginFill('#BBBBBB');
    board.graphics.drawRect(0, 0, canvas.width, 50);
    board.y = 0;
    board.name = "instruction_board";
    stage.addChild(board);

    label = new createjs.Text(message, "30px Handlee", "#000000");
    // label.alpha = 1;
    label.textAlign = 'center';
    // label.textBaseline = "top";
    label.x = canvas.width / 2;
    label.y = 10;
    label.name = "instruction_label";
    stage.addChild(label);
}


function optimizeForTouchAndScreens() {
    if (window.orientation !== 'undefined') {
        window.onorientationchange = onOrientationChange;
        if (createjs.Touch.isSupported()) {
            createjs.Touch.enable(stage);
        }
        onOrientationChange();
    } else {
        window.onresize = resizeGame;
        resizeGame();
    }
}


function onOrientationChange() {
    setTimeout(resizeGame, 100);
}


function resizeGame() {
    var nTop, nLeft, scale;
    var gameWrapper = document.getElementById('gameWrapper');
    var bg = document.getElementById('bg');
    var w = window.innerWidth;
    var h = window.innerHeight;
    var nWidth = window.innerWidth;
    var nHeight = window.innerHeight;
    var widthToHeight = canvas.width / canvas.height;
    var nWidthToHeight = nWidth / nHeight;
    if (nWidthToHeight > widthToHeight) {
        nWidth = nHeight * widthToHeight;
        scale = nWidth / canvas.width;
        nLeft = (w / 2) - (nWidth / 2);
        gameWrapper.style.left = (nLeft) + "px";
        gameWrapper.style.top = "0px";
    } else {
        nHeight = nWidth / widthToHeight;
        scale = nHeight / canvas.height;
        nTop = (h / 2) - (nHeight / 2);
        gameWrapper.style.top = (nTop) + "px";
        gameWrapper.style.left = "0px";
    }
    canvas.setAttribute("style", "-moz-transform:scale(" + scale + "); -ms-transform:scale(" + scale + "); -webkit-transform:scale(" + scale + "); transform:scale(" + scale + "); ");
    bg.setAttribute("style", "-moz-transform:scale(" + scale + "); -ms-transform:scale(" + scale + "); -webkit-transform:scale(" + scale + "); transform:scale(" + scale + "); ");
    window.scrollTo(0, 0);
}

/* --- Cookies --- */

function setCookie(cname, cvalue, exdays) {
    var d = new Date(),
        date_string;
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    date_string = d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; expires=" + date_string;
}


function getCookie(cname) {
    var name = cname + "=", // Text to search for
        cookie_list = document.cookie.split(';'),
        i,
        cookie;
    for (i = 0; i < cookie_list.length; i++) {
        cookie = cookie_list[i];
        while (cookie.charAt(0) === ' ') { cookie = cookie.substring(1); }
        if (cookie.indexOf(name) === 0) { return cookie.substring(name.length, cookie.length); }
    }
    return "";
}


function incrementScore(increment) {
    var i,
        coin,
        newX = 96 + (score + 1) * (canvas.width - 96) / (MAX_SCORE + 1), // 96: arrow_left.width
        newY = canvas.height - 25;  // canvas.height - coinBoard.height/2

    if (increment > 0) {
        ++score;
        if (score <= MAX_SCORE) {
            if (score <= 5) {
                coin = new createjs.Bitmap("../img/bronze.png");
            } else if (score <= 10) {
                coin = new createjs.Bitmap("../img/silver.png");
            } else {
                coin = new createjs.Bitmap("../img/gold.png");
            }

            coin.name = "coin" + score;
            coin.regX = 20; // coin.image.width / 2;
            coin.regY = 20; // coin.image.height / 2;
            coin.x = stage.mouseX;
            coin.y = stage.mouseY;
            stage.addChild(coin);
            createjs.Tween.get(coin).to({x: newX, y: newY}, 500, createjs.Ease.QuadOut);

            incrementScore(increment - 1);
        }
    } else {
        // var mySound = createjs.Sound.createInstance("success");
        // mySound.play();
        // src, [interrupt="none"|options], [delay=0], [offset=0], [loop=0], [volume=1], [pan=0], [startTime=null], [duration=null]
        createjs.Sound.play("success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
    }
}

function decrementScore() {
    if (score > 0) {
        var coin = stage.getChildByName("coin" + score);
        // createjs.Tween.get(coin).to({x: stage.mouseX, y: stage.mouseY}, 500, createjs.Ease.QuadOut).call(function () {
        createjs.Tween.get(coin).to({y: -20}, 500, createjs.Ease.QuadOut).call(function () {
            stage.removeChild(coin);
        });
        --score;
    }
    createjs.Sound.play("error", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
}


function showCoinBoard() {
    var board, arrow;

    board = new createjs.Shape();
    board.alpha = 0.5;
    board.graphics.beginFill('#BBBBBB');
    board.graphics.drawRect(0, 0, canvas.width, 50);
    board.y = canvas.height - 50;
    board.name = "coin_board";
    stage.addChild(board);

    arrow = new createjs.Bitmap("../img/left_arrow.png");
    arrow.x = 0;
    arrow.y = canvas.height - 45;
    arrow.addEventListener("click", function () {
        window.location.assign('../index.html');
    });
    stage.addChild(arrow);
}


function showCurrentCoins() {
    var i, coin;
    showCoinBoard();
    for (i = 1; i <= score; ++i) {
        if (i <= 5) {
            coin = new createjs.Bitmap("../img/bronze.png");
        } else if (i <= 10) {
            coin = new createjs.Bitmap("../img/silver.png");
        } else {
            coin = new createjs.Bitmap("../img/gold.png");
        }

        coin.name = "coin" + i;
        coin.regX = 20; // coin.image.width / 2;
        coin.regY = 20; // coin.image.height / 2;
        coin.x = i * canvas.width / (MAX_SCORE + 1);
        coin.y = canvas.height - 25;  // canvas.height - coinBoard.height/2
        stage.addChild(coin);
    }
}


function loadInstruction(list, quiet) {
    quiet = quiet || false;
    var manifest = [];
    list.forEach(function (sound) {
        manifest.push({id: sound, src : "../instructions/" + sound + ".opus", data: 1});
    });
    var queue = new createjs.LoadQueue();
    queue.installPlugin(createjs.Sound);
    createjs.Sound.alternateExtensions = ["ogg", "mp3"];

    queue.addEventListener("complete", function () {
        if (quiet) {
            instructionsLoaded();
        } else {
            createjs.Sound.play(list[0], createjs.Sound.INTERRUPT_ANY, 0, 0, 0, 1, 0);
        }
    });

    queue.loadManifest(manifest);
}


function shuffle(list) {
    var i,
        shuffled_list = [];

     while (list.length > 0) {
        i =  Math.floor(Math.random() * list.length);
        shuffled_list.push(list[i]);
        list.splice(i, 1);
    }
    return shuffled_list;
}


function selectItem(list, old_item) {
    var i,
        item;
    do {
        i = Math.floor(Math.random() * list.length);
        item = list[i];
    } while (item === old_item);
    return item;
}


function selectItems(amount, list) {
    var item,
        items = [];
    while (items.length < amount) {
        item = selectItem(list);
        if (items.indexOf(item) === -1) {
            items.push(item);
        }
    }
    return items;
}


function loadImages(list, action, path, extension) {
    var manifest = [];
    path = path || "../img/";
    extension = extension || ".png";
    list.forEach(function (item) {
        manifest.push({id: item, src : path + item + extension});
    });
    queue = new createjs.LoadQueue();
    queue.addEventListener("complete", action);
    queue.loadManifest(manifest);
}
