var square = function (color, zoom) {
    var shape = new createjs.Shape();
    zoom = zoom || 1;
    shape.graphics.beginStroke('#000');
    if (color) { shape.graphics.beginFill(color); }
    shape.graphics.drawRect(0, 0, 100 * zoom, 100 * zoom);
    shape.regX = 50 * zoom;
    shape.regY = 50 * zoom;
    shape.name = "square";
    shape.factor = zoom;
    return shape;
};

var rectangle = function (color, zoom) {
    var shape = new createjs.Shape();
    zoom = zoom || 1;
    shape.graphics.beginStroke('#000');
    if (color) { shape.graphics.beginFill(color); }
    shape.graphics.drawRect(0, 0, 130 * zoom, 80 * zoom);
    shape.regX = 65 * zoom;
    shape.regY = 40 * zoom;
    shape.name = "rectangle";
    shape.factor = zoom;
    return shape;
};

var circle = function (color, zoom) {
    var shape = new createjs.Shape();
    zoom = zoom || 1;
    shape.graphics.beginStroke('#000');
    if (color) { shape.graphics.beginFill(color); }
    shape.graphics.drawCircle(0, 0, 50 * zoom);
    shape.name = "circle";
    shape.factor = zoom;
    return shape;
};

var star = function (color, zoom) {
    var shape = new createjs.Shape();
    zoom = zoom || 1;
    shape.graphics.beginStroke('#000');
    if (color) { shape.graphics.beginFill(color); }
    shape.graphics.drawPolyStar(0, 0, 60 * zoom, 5, 0.62, 54);
    shape.name = "star";
    shape.factor = zoom;
    return shape;
};

var triangle = function (color, zoom) {
    var shape = new createjs.Shape();
    zoom = zoom || 1;
    shape.graphics.beginStroke('#000');
    if (color) { shape.graphics.beginFill(color); }
    shape.graphics.moveTo(50 * zoom, 0).
        lineTo(0, 100 * zoom).
        lineTo(100 * zoom, 100 * zoom).
        lineTo(50 * zoom, 0);
    shape.name = "triangle";
    shape.regX = 50 * zoom;
    shape.regY = 50 * zoom;
    shape.factor = zoom;
    return shape;
};
