var MAX_SCORE = 15,
    sample_card,
    sample_word,
    sample_words = [].
        concat(ALL_WORDS['a']).
        concat(ALL_WORDS['e']).
        concat(ALL_WORDS['i']).
        concat(ALL_WORDS['o']).
        concat(ALL_WORDS['u']),
    score = 0;
var canvas, queue, stage;


function selectWord(old_word) {
    var i,
        word;
    do {
        var i = Math.floor(Math.random() * sample_words.length);
        word = sample_words[i];
    } while (word === old_word);
    return word;
}

function setupStage() {
    var i,
        card,
        cards = buildLetterCards(sample_word[0]),
        xPos = canvas.width / 4;
        yPos = canvas.height * 3 / 4;

    // var re = new RegExp(missing_letter, 'g');
    // edited_word = word.replace(re, '_');
    sample_card = buildCard(sample_word, '_' + sample_word.substr(1, sample_word.length));
    sample_card.x = canvas.width / 2;
    sample_card.y = canvas.height / 3;
    stage.addChild(sample_card);
    stage.setChildIndex(sample_card, 0);

    while (cards.length > 0) {
        i =  Math.floor(Math.random() * cards.length);
        card = cards[i];
        card.x = -200;
        card.y = 0;
        card.rotation = Math.random() * 600;
        card.addEventListener('click', checkLetterCard);
        stage.addChild(card);
        stage.setChildIndex(card, 0);
        createjs.Tween.get(card).
            wait(i * 100).
            to({x: xPos, y: yPos, rotation: 0}, 300);
        cards.splice(i, 1);
        xPos += canvas.width / 4;
    }
}


function buildLetterCards(letter) {
    var card,
        cards = [],
        letters = randomVowels(2, letter);
    letters.push(letter);

    letters.forEach(function (letter, i) {
        card = buildLetterCard(letter);
        card.name = "lettercard" + i;
        cards.push(card);
    });

    return cards;
}


function checkLetterCard(e) {
    var card = e.currentTarget;
    if (card.key === sample_card.key[0]) {
        nextWord();
    } else {
        decrementScore();
    }
}

function nextWord() {
    stage.removeChild(sample_card);
    stage.removeChild(stage.getChildByName("lettercard0"));
    stage.removeChild(stage.getChildByName("lettercard1"));
    stage.removeChild(stage.getChildByName("lettercard2"));

    incrementScore(2);
    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../syllablediscrimination1/' + ORDER[0] + '.html');
        });
    } else {
        sample_word = selectWord(sample_word);
        loadMedia([sample_word]);
    }
}


function loadMedia(list) {
    var manifest = [{id: "emptycard", src: "../img/emptycard.png"}];
    list.forEach(function (word) {
        manifest.push({id: word, src : "../img/" + word + ".png"}); // FIXME: asciify src
    });
    queue = new createjs.LoadQueue();
    queue.addEventListener("complete", setupStage);
    queue.loadManifest(manifest);
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    createjs.Ticker.setFPS(60);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    sample_word = selectWord();
    showInstruction("Pulsa la letra que falta.");
    showCoinBoard();
    loadMedia([sample_word]);
    startGame();
}
