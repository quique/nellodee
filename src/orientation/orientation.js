var MAX_SCORE = 15,
    instructions = ["pulsa_el_dibujo_de_arriba", "pulsa_el_dibujo_de_la_derecha", "pulsa_el_dibujo_de_abajo",
        "pulsa_el_dibujo_de_la_izquierda", "pulsa_el_dibujo_del_centro"],
    names = ["de arriba.", "de la derecha.", "de abajo.", "de la izquierda.", "del centro."],
    score = 0,
    goal_direction,
    factor = 1.5,
    canvas,
    stage;

function goalDirection() {
    var i = Math.floor(Math.random() * names.length);
    return names[i];
}


function checkDirection(e) {
    if (e.target.name === goal_direction) {
        incrementScore(2);
        if (score > MAX_SCORE) {
            if (getCookie("case") === "") {
                setCookie("case", "uppercase", 365);
            }
            var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
            instance.on("complete", function () {
                window.location.assign('../auraldiscrimination/a.html');
            });
        } else {
            newInstruction();
        }
    } else {
        decrementScore();
    }
}


function newInstruction() {
    var old_direction = goal_direction;
    do {
        goal_direction = goalDirection();
    } while (goal_direction === old_direction);
    stage.removeChild(stage.getChildByName("instruction_label"));
    stage.removeChild(stage.getChildByName("instruction_board"));
    showInstruction("Pulsa el dibujo " + goal_direction);
    createjs.Sound.play(instructions[names.indexOf(goal_direction)], createjs.Sound.INTERRUPT_ANY, 0, 0, 0, 1, 0);
}


function showLayout() {
    var shapes = [triangle, triangle, triangle, triangle, square],
        colors = ["white", "red", "yellow", "black", "green"],
        midX = canvas.width / 2,
        midY = canvas.height / 2,
        offset = 250,
        xPos = [midX, midX + offset, midX, midX - offset, midX],
        yPos = [midY - offset, midY, midY + offset, midY, midY],
        i,
        direction;

    for (i = 0; i < shapes.length; i += 1) {
        direction = shapes[i](colors[i], factor);
        direction.name = names[i];
        direction.x = xPos[i];
        direction.y = yPos[i];
        direction.rotation = i * 90;
        direction.addEventListener('click', checkDirection);
        stage.addChild(direction);
    }

    newInstruction();
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    //createjs.Ticker.setFPS(60);
}


function instructionsLoaded() {
    showLayout();
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    loadInstruction(instructions, true);
    showCoinBoard();
    startGame();
}
