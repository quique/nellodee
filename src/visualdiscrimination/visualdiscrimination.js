var COLORS = ['red', 'orange', 'yellow', 'green', 'aqua', 'blue', 'purple'];
var MAX_SCORE = 15;
var FACTOR = 1.5;

var canvas, stage;
var slots = [];
var score = 0;



function startDrag(e) {
    var shape = e.target,
        slot;
    stage.setChildIndex(shape, stage.getNumChildren() - 1);

    stage.addEventListener('stagemousemove', function (e) {
        shape.x = e.stageX;
        shape.y = e.stageY;
    });

    stage.addEventListener('stagemouseup', function (e) {
        stage.removeAllEventListeners();
        slot = slots[shape.slotNr];
        var relpoint = slot.globalToLocal(stage.mouseX, stage.mouseY);

        if (slot.hitTest(relpoint.x, relpoint.y)) {
            shape.removeEventListener("mousedown", startDrag);
            createjs.Tween.get(shape).to({x: slot.x, y: slot.y}, 200, createjs.Ease.quadOut).call(newPiece);
        } else {
            createjs.Tween.get(shape).to({x: shape.homeX, y: shape.homeY}, 200, createjs.Ease.quadOut);
            createjs.Sound.play("so-so", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        }
    });
}


function showLayout() {
    var shapes = [square, rectangle, circle, star, triangle],
        tempshapes = [].concat(shapes),
        c = Math.floor(Math.random() * COLORS.length),
        s = Math.floor(Math.random() * shapes.length),
        piece = shapes[s](COLORS[c], FACTOR),
        i,
        sl,
        slot;

    //piece.name = "sample";
    piece.x = canvas.width / 2;
    piece.y = canvas.height * 3 / 4;
    piece.homeX = piece.x;
    piece.homeY = piece.y;
    piece.addEventListener("mousedown", startDrag);
    stage.addChild(piece);
    stage.setChildIndex(piece, 0);

    for (i = 0; i < shapes.length; ++i) {
        sl = Math.floor(Math.random() * tempshapes.length);
        slot = tempshapes[sl]('white', FACTOR);
        tempshapes.splice(sl, 1);
        slot.x = (i + 1) * canvas.width / (shapes.length + 1);
        slot.y = canvas.height / 4;
        stage.addChild(slot);
        stage.setChildIndex(slot, 0);
        slots.push(slot);
        if (slot.name === piece.name) {
            piece.slotNr = i;
            piece.name = "sample";
        }
    }
}


function newPiece(e) {
    slots.forEach(function (slot) {
        stage.removeChild(slot);
    });
    stage.removeChild(stage.getChildByName("sample"));

    incrementScore(2);
    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../visualdiscrimination2/index.html');
        });
    } else {
        showLayout();
    }
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    //createjs.Ticker.setFPS(60);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Arrastra la figura a su hueco.");
    loadInstruction(["arrastra_la_figura_a_su_hueco"]);
    showCoinBoard();

    showLayout();
    startGame();
}
