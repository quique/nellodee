var COLORS = ['red', 'orange', 'yellow', 'green', 'aqua', 'blue', 'purple'];
var MAX_SCORE = 15;
var FACTOR = 1.5;

var canvas, stage;
var slot;
var score = 0;


function showPiece() {
    var shapes = [square, rectangle, circle, star, triangle],
        s = Math.floor(Math.random() * shapes.length),
        c = Math.floor(Math.random() * COLORS.length),
        piece = shapes[s](COLORS[c], FACTOR);

    slot = shapes[s]('white', FACTOR);
    slot.x = Math.floor(Math.random() * (canvas.width - 150)) + 75;
    slot.y = Math.floor(Math.random() * (canvas.height - 250)) + 125;
    stage.addChild(slot);

    do {
        piece.x = Math.floor(Math.random() * (canvas.width - 150)) + 75;
        piece.y = Math.floor(Math.random() * (canvas.height - 250)) + 125;
    } while (Math.abs(piece.x - slot.x) < 200 && Math.abs(piece.y - slot.y) < 200);
    piece.homeX = piece.x;
    piece.homeY = piece.y;
    piece.addEventListener("mousedown", startDrag);
    stage.addChild(piece);
}


function startDrag(e) {
    var shape = e.target;
    stage.setChildIndex(shape, stage.getNumChildren() - 1);

    stage.addEventListener('stagemousemove', function (e) {
        shape.x = e.stageX;
        shape.y = e.stageY;
    });

    stage.addEventListener('stagemouseup', function (e) {
        stage.removeAllEventListeners();
        var relpoint = slot.globalToLocal(stage.mouseX, stage.mouseY);
        if (slot.hitTest(relpoint.x, relpoint.y)) {
            shape.removeEventListener("mousedown", startDrag);
            createjs.Tween.get(shape).to({x: slot.x, y: slot.y}, 200, createjs.Ease.quadOut).call(newPiece);
        } else {
            createjs.Tween.get(shape).to({x: shape.homeX, y: shape.homeY}, 200, createjs.Ease.quadOut);
            createjs.Sound.play("so-so", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        }
    });
}


function newPiece(e) {
    stage.removeChild(e.target);
    stage.removeChild(slot);
    incrementScore(2);
    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../visualdiscrimination/index.html');
        });
    } else {
        showPiece();
    }
}


function startGame() {
    showPiece();
    //createjs.Ticker.timingMode = createjs.Ticker.RAF;
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    //createjs.Ticker.setFPS(60);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Arrastra la figura al hueco en blanco.");
    loadInstruction(["arrastra_la_figura_al_hueco_en_blanco"]);
    showCoinBoard();

    startGame();
}
