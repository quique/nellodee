var MAX_SCORE = 15,
    NUM_WORDS = 6,
    matches = 0,
    squares = [],
    squares_flipped = [],
    score = 0,
    words = [];
var canvas, queue, stage;


function resetFlippedSquares() {
    squares_flipped[0].mouseEnabled = squares_flipped[1].mouseEnabled = true;
    squares_flipped[0].getChildByName('fg').visible = false;
    squares_flipped[1].getChildByName('fg').visible = false;
    squares_flipped = [];
}


function evalGame() {
    var instance;
    if (matches === NUM_WORDS) {
        instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_ANY, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../orientation2/index.html');
        });
    } else {
        squares_flipped = [];
    }
}


function evalSquaresFlipped() {
    if (squares_flipped[0].key === squares_flipped[1].key) {
        ++matches;
        incrementScore(matches < 4 ? 3 : 2);
        evalGame();
    } else {
        window.setTimeout(resetFlippedSquares, 1000);
    }
}


function checkSquare(event) {
    var square = event.currentTarget;

    if (squares_flipped.length === 2) {
        return;
    }

    square.mouseEnabled = false;
    square.getChildByName('fg').visible = true;
    squares_flipped.push(square);
    if (squares_flipped.length === 2) {
        evalSquaresFlipped();
    }
}


function setupStage() {
    var xPos = canvas.width / 5,
        yPos = canvas.height / 4;

    squares.forEach(function (square, i) {
        square.x = -200;
        square.y = 0;
        square.rotation = Math.random() * 600;
        stage.addChild(square);

        createjs.Tween.get(square)
            .wait(i * 100)
            .to({x: xPos, y: yPos, rotation: 0}, 300);
        xPos += canvas.width / 5;
        if (xPos > canvas.width * 4 / 5) {
            xPos = canvas.width / 5;
            yPos += canvas.height / 4;
        }
    });
}


function buildSquares(words) {
    var square,
        square2;

    words.forEach(function (word) {
        square = buildSquare(word);
        square.getChildByName('fg').visible = false;
        square2 = square.clone(true);
        square2.key = square.key;
        square2.addEventListener("click", checkSquare);
        squares.push(square, square2);
    });
    squares = shuffle(squares);
    return squares;
}


function startGame() {
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
    createjs.Ticker.setFPS(50);
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Busca la pareja de cada dibujo.");
    loadInstruction(["busca_la_pareja_de_cada_dibujo"]);
    showCoinBoard();

    words = selectItems(NUM_WORDS, FRUITS);
    squares = buildSquares(words);
    setupStage();

    startGame();
}
