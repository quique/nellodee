var COLORS = ['red', 'orange', 'yellow', 'green', 'aqua', 'blue', 'purple'];
var MAX_SCORE = 15;

var canvas, stage;
var factor = 2;
var score = 0;


function showShape() {
    var shapes = [square, rectangle, circle, star, triangle],
        s = Math.floor(Math.random() * shapes.length),
        c = Math.floor(Math.random() * COLORS.length),
        shape = shapes[s](COLORS[c], factor);
    shape.x = Math.floor(Math.random() * (canvas.width - 200)) + 100;
    shape.y = Math.floor(Math.random() * (canvas.height - 300)) + 150;
    shape.addEventListener('click', shapeClicked);
    stage.addChild(shape);
}


function shapeClicked(event) {
    stage.removeChild(event.target);
    incrementScore(2);

    if (score > MAX_SCORE) {
        var instance = createjs.Sound.play("big-success", createjs.Sound.INTERRUPT_NONE, 0, 0, 0, 1, 0);
        instance.on("complete", function () {
            window.location.assign('../activity02/index.html');
        });
    } else {
        factor -= 0.125;
        showShape();
    }
}


function init() {
    canvas = document.getElementById('canvas');
    stage = new createjs.Stage(canvas);
    optimizeForTouchAndScreens();

    showInstruction("Pulsa sobre la figura.");
    loadInstruction(["pulsa_sobre_la_figura"]);
    showCoinBoard();
    showShape();

    createjs.Ticker.timingMode = createjs.Ticker.RAF;
    createjs.Ticker.addEventListener("tick", function () {
        stage.update();
    });
}
