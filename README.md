Cartilla Ilustrada
==================

Actividades para niños de Educación infantil o primer ciclo de Primaria, para facilitar el aprendizaje de la lectura y la escritura.


Instalación
-----------

Simplemente copia el contenido de la carpeta `src' a un servidor web, y utiliza tu navegador.
El navegador debe tener Javascript activado.  No hay más dependencias: ni plugin de Flash, ni Java, ni PHP o ninguna otra cosa.

